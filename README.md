# HODL - your favourite Cryptocurrency Assistant #
Kriptovalute so digitalna sredstva, ki so bila ustvarjena kot medij izmenjave, ki s pomočjo kriptografije zagotavlja varne
transakcije, kontrolira nastanek novih enot in preverja izmenjave teh sredstev. Čeprav so s kriptovalutam ekspirementirali že v poznih 90-ih, so
se prve decentralizirane kriptovalute pojavile leta 2009 (Bitcoin). Dandanes jih je že več kot 1000, okoli 600 pa jih ima tržni delež (angl. market cap) večiji kot 100,000$!
Prav zaradi bizarnih številk se čedalje več ljudi vključuje v to "industrijo". Želja po enormnem zaslužku je vzrok za množično vlaganje v različne kriptovalute,
katerih vrednost lahko zraste v "nebo" (angl. "To the Moon") ali pa pade.

Tu vstopimo mi! Ime naše skupine izhaja iz tipkarske napake nesrečnega vlagatelja, ki je na bitcoin forumu, pod psevdonimom GameKyuubi,
ustvaril temo I AM HODLING in zapisal nekaj besed o "[hodlanju](https://bitcointalk.org/index.php?topic=375643.0)". Da ne bi prihajalo
do podobnih incidentov bomo zato naredili spletno stran HODL, ki bo vaš asistent v trgovanju.

### Opis funkcionalnosti ###

#### Interaktivnost ####
Na začetni strani je prikazan seznam kriptovalut. Uporabniku se ob kliku na vrstico odpre nova stran, 
na kateri je več podrobnosti o izbrani valuti, med katerimi je tudi graf, ki predstavlja zgodovino spreminjanja cene.
Tudi sam graf je interaktiven, saj lahko s pomikanjem miške opazujemo ceno v daljni preteklosti. Prav tako lahko na
graf dodate nekaj indikatorjev, ki pomagajo pri tehnični analizi. Uporabnik lahko nastavi, da se mu prikaže obvestilo, ko izbrani kovanec doseže
določeno ceno.

#### Komunikacija z oddaljenimi storitvami ####
Spletna stran prejema podatke o kovancih preko REST API-jev (zgodovina cene) in Websocket-ov (trenutna tržna cena). Zavoljo tega projekta smo se omejili zgolj
na kovance, ki kotirajo na borzi Bittrex ([API dokumentacija](https://bittrex.com/home/api)).

#### Dovolj zapleteno procesiranje podatkov ####
Indikatorji na grafu ter upravljanje obvestil pripomoreta k večji kompleksnosti spletne strani z vidika 
procesiranja podatkov, saj je potrebno vrednosti vseh indikatorjev preračunati glede na pridobljene podatke s 
strani borze.

![Postavitev strani](https://i.imgur.com/Emzvihk.png "Primer postavitve strani")

### How do I get set up? (TODO) ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Delujoče funkcionalnosti (označeno z *) ###

* [ ] prikazan seznam kriptovalut
* [ ] klik na vrstico odpre novo stran s podrobnostmi
* [ ] graf spreminjanja cene
* [ ] dinamika grafa
* [ ] dodajanje indikatorjev
* [ ] obvestila ob prekoračitvi cene

### Kje nas lahko kontaktirate? ###

* CEO - stefanic.gregor@gmail.com | gs4267@student.uni-lj.si
* COO - alen.jursic1997@gmail.com | aj2403@student.uni-lj.si
* CMO - samo8.okorn@gmail.com	| so0164@student.uni-lj.si