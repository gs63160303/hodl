module Graph exposing (..)
import History exposing (..)
import Html exposing (..)
import Html.Events exposing (onClick)
import Html.Attributes exposing (attribute)
import Svg
import Svg.Attributes exposing (..)
import Round exposing (..)

-- Candle 1.Open, 2.Closed, 3.Max, 4.Min
type Candle =
    Empty | Candle Float Float Float Float

convert : List History.Candlestick -> List Candle
convert lcs =
    let
        f : History.Candlestick -> Candle
        f cs =
            Candle
            (cs.open)
            (cs.close)
            (cs.high)
            (cs.low)
    in
        List.map (f) (lcs)

{-===============================================================================-}
{-=================================== EMA =======================================-}
{-===============================================================================-}
drawEMA : List Float -> Int -> Float -> Float -> List(Svg.Svg msg)
drawEMA l stTock width height =
    let
        narisiEMO : List Float -> Int -> Float -> Float -> Float -> Float -> Float -> Float -> List(Svg.Svg msg)
        narisiEMO l stTock widthW heightH sirina low high posX =
            if (List.isEmpty l) then
              []
            else if (List.isEmpty (Maybe.withDefault [] (List.tail l))) then
              []
            else if(stTock==0) then
              []
            else
              if (((Maybe.withDefault 0 (List.head l))==(-1)) || ((Maybe.withDefault 0 (List.head (Maybe.withDefault [] (List.tail l))))==(-1))) then
                []++(narisiEMO (Maybe.withDefault [] (List.tail l)) (stTock-1) widthW heightH sirina low high (posX+sirina))
              else
                  [Svg.line [
                            x1 (toString (posX+(sirina/2))) ,
                            x2 (toString (posX+sirina+(sirina/2))),
                            y1 (toString (heightH - (((Maybe.withDefault 0 (List.head l))-low)*((height-10)/(high-low))))),
                            y2 (toString (heightH - (((Maybe.withDefault 0 (List.head (Maybe.withDefault [] (List.tail l))))-low)*((height-10)/(high-low))))), stroke "blue"] []
                            ]
                  ++(narisiEMO (Maybe.withDefault [] (List.tail l)) (stTock-1) widthW heightH sirina low high (posX+sirina))
    in
        narisiEMO l stTock width height (getWidth stTock width) (lowSearch l) (highSearch l) 5

drawSMA : List Float -> Int -> Float -> Float -> List(Svg.Svg msg)
drawSMA l stTock width height =
    let
        narisiSMA : List Float -> Int -> Float -> Float -> Float -> Float -> Float -> Float -> List(Svg.Svg msg)
        narisiSMA l stTock widthW heightH sirina low high posX =
            if (List.isEmpty l) then
              []
            else if (List.isEmpty (Maybe.withDefault [] (List.tail l))) then
              []
            else if(stTock==0) then
              []
            else
              if (((Maybe.withDefault 0 (List.head l))==(-1)) || ((Maybe.withDefault 0 (List.head (Maybe.withDefault [] (List.tail l))))==(-1))) then
                  []++(narisiSMA (Maybe.withDefault [] (List.tail l)) (stTock-1) widthW heightH sirina low high (posX+sirina))
              else
                  [Svg.line [
                          x1 (toString (posX+(sirina/2))) ,
                          x2 (toString (posX+sirina+(sirina/2))),
                          y1 (toString (heightH - (((Maybe.withDefault 0 (List.head l))-low)*((height-10)/(high-low))))),
                          y2 (toString (heightH - (((Maybe.withDefault 0 (List.head (Maybe.withDefault [] (List.tail l))))-low)*((height-10)/(high-low))))), stroke "purple"] []
                          ]
                  ++(narisiSMA (Maybe.withDefault [] (List.tail l)) (stTock-1) widthW heightH sirina low high (posX+sirina))

    in
        narisiSMA l stTock width height (getWidth stTock width) (lowSearch l) (highSearch l) 5

lowSearch : List Float -> Float
lowSearch l =
    let findLow l lowest =
        if (List.isEmpty l) then
          lowest
        else if(((Maybe.withDefault 9999 (List.head l))<lowest) && ((Maybe.withDefault 0 (List.head l))/=(-1))) then
          findLow (Maybe.withDefault [] (List.tail l)) (Maybe.withDefault 9999 (List.head l))
        else
          findLow (Maybe.withDefault [] (List.tail l)) lowest
    in
      findLow l 99999999

highSearch : List Float -> Float
highSearch l =
    let findHigh l highest =
        if (List.isEmpty l) then
          highest
        else if(((Maybe.withDefault 0 (List.head l))>highest) && ((Maybe.withDefault 0 (List.head l))/=(-1))) then
          findHigh (Maybe.withDefault [] (List.tail l)) (Maybe.withDefault 0 (List.head l))
        else
          findHigh (Maybe.withDefault [] (List.tail l)) highest
    in
      findHigh l 0

drawEIS : List Int -> Int -> Float -> Float -> List(Svg.Svg msg)
drawEIS l st widthW heightH =
    let
        narisiEIS : List Int -> Int -> Float -> Float -> Float -> Float -> Float -> List(Svg.Svg msg)
        narisiEIS l st widthW heightH sirina posX visina =
          if (List.isEmpty l) then
              []
          else if (st==0) then
              []
          else
            if ((Maybe.withDefault 0 (List.head l))<0) then
                [Svg.rect [x (toString posX), y (toString heightH), width (toString sirina), height (toString visina),stroke "red", fill "red"] []]
                ++(narisiEIS (Maybe.withDefault [] (List.tail l)) (st-1) widthW heightH sirina (posX+sirina) visina)
            else if ((Maybe.withDefault 0 (List.head l))==0) then
                [Svg.rect [x (toString posX), y (toString heightH), width (toString sirina), height (toString visina),stroke "blue", fill "blue"] []]
                ++(narisiEIS (Maybe.withDefault [] (List.tail l)) (st-1) widthW heightH sirina (posX+sirina) visina)
            else
                [Svg.rect [x (toString posX), y (toString heightH), width (toString sirina), height (toString visina),stroke "green", fill "green"] []]
                ++(narisiEIS (Maybe.withDefault [] (List.tail l)) (st-1) widthW heightH sirina (posX+sirina) visina)

    in
        Svg.rect [x (toString 0), y (toString (heightH+10)), rx (toString 10), ry (toString 10), width (toString widthW), height (toString (54)), stroke "black" ,fill "none"] []
        :: (narisiEIS l st widthW (heightH+12) ((getWidth st widthW)+4) 5 50)

drawVerticalLines : List Bool -> Int -> Float -> Float -> List(Svg.Svg msg)
drawVerticalLines l st widthW heightH =
    let drawVert l st widthW heightH sirina posX =
        if (List.isEmpty l) then
            []
        else if (st==0) then
            []
        else
          if ((Maybe.withDefault False (List.head l))==True) then
              [Svg.line [x1 (toString (posX+(sirina/2))),x2 (toString (posX+(sirina/2))), y1 (toString heightH) ,y2 (toString 0), stroke "gray", strokeDasharray "5, 5"] []]
              ++(drawVert (Maybe.withDefault [] (List.tail l)) (st-1) widthW heightH sirina (posX+sirina))
          else
            []++(drawVert (Maybe.withDefault [] (List.tail l)) (st-1) widthW heightH sirina (posX+sirina))

    in
        drawVert l st widthW heightH (getWidth st widthW) 5

{-===============================================================================-}
{-============================= IZRIS GRAFA =====================================-}
{-===============================================================================-}

drawScale : Float -> Float -> Float -> Float ->List (Svg.Svg msg)
drawScale high low widthW heightH =
    let
        makeScale : Int -> Float -> Float -> Float -> Float -> Float -> List (Svg.Svg msg)
        makeScale st high low zamik trenutno stevilka =
            if (st>1)then
                [Svg.line [x1 (toString (5)),x2 (toString (widthW)), y1 (toString (trenutno-zamik)) ,y2 (toString (trenutno-zamik)), stroke "gray",strokeDasharray "5, 5"] [],
                 Svg.line [x1 (toString (widthW)),x2 (toString (widthW+30)), y1 (toString (trenutno-zamik)) ,y2 (toString (trenutno-zamik)), stroke "black",strokeWidth "3"] [],
                 Svg.text_ [ dx (toString (widthW+45)), dy (toString ((trenutno-zamik)+5)) ] [ Svg.text (Round.round 8 (low+stevilka)) ]
                ]
                ++(makeScale (st-1) high (low+stevilka) zamik (trenutno-zamik) stevilka)
            else
                []
    in
        makeScale 7 high low ((heightH+10)/7) (heightH+10) ((high-low)/7)


--funkcija, ki narise graf
draw : List Candle -> Int -> Float ->Float ->List (Svg.Svg msg)
draw l stSveck width height =
   let
       narisi : Int -> List Candle -> Float -> Float -> Float -> Float -> Float -> Float -> Float -> List (Svg.Svg msg)
       narisi st list posX posY sirina high low width height =
           if(List.isEmpty list) then
               [(drawBorder width height)]++(drawScale high low (width-30) height)
           else if (st==0) then
               [(drawBorder width height)]++(drawScale high low (width-30) height)
           else
               case (Maybe.withDefault Empty (List.head list)) of
                   Empty -> []
                   Candle o c min max ->
                           [(drawLine sirina posX posY min max high low height), (drawCandle o c posX posY sirina high low height)] ++ (narisi (st-1) (Maybe.withDefault [] (List.tail list)) (posX+sirina+4) posY sirina high low width height)
    in
        case l of
            [] ->
                [Svg.line [x1 "1", x2 "1", y1 "1", y2 "1"] []]
            _ ->
                narisi stSveck l 5 height (getWidth stSveck width) (getHighest l stSveck) (getLowest l stSveck) width height


drawBorder : Float -> Float -> (Svg.Svg msg)
drawBorder widthW heightH =
    Svg.rect [x (toString 0), y (toString 0), rx (toString 10), ry (toString 10), width (toString widthW), height (toString (heightH+10)), stroke "black" ,fill "none"] []


drawCandle : Float -> Float -> Float -> Float -> Float -> Float -> Float -> Float -> (Svg.Svg msg)
drawCandle o c posX posY sirina high low heightH=
    let
        a = o-low
        b = c-low
        visina = (heightH-10)/(high-low)
    in
        if (a>b)then
            Svg.rect [x (toString posX), y (toString (posY-(a*visina))), width (toString sirina), height (toString ((a-b)*visina)), stroke "black" ,fill "rgb(206, 67, 16)"] []
        else if (a<b) then
            Svg.rect [x (toString posX), y (toString (posY-(b*visina))), width (toString sirina), height (toString ((b-a)*visina)), stroke "black" ,fill "rgb(36, 181, 28)"] []
        else
            Svg.line [x1 (toString posX),x2 (toString (posX+sirina)), y1 (toString (posY-(a*visina))) ,y2 (toString (posY-(a*visina))), stroke "black"] []

--funkcija, ki vrne faktor za razmerje �irine
getWidth : Int -> Float -> Float
getWidth stSveck width =
    ((width-50)-toFloat(stSveck*4))/(toFloat(stSveck))

getLowest : List Candle -> Int -> Float
getLowest l stSveck =
    let najdi l minimum st =
         if(List.isEmpty l)then
            minimum
        else if(st==0) then
            minimum
        else
            case (Maybe.withDefault Empty (List.head l)) of
               Empty -> minimum
               Candle o c max min ->
                   if(o<=c && o<=max && o<=min) then
                       if(o<minimum) then
                           najdi (Maybe.withDefault [] (List.tail l)) o (st-1)
                       else
                           najdi (Maybe.withDefault [] (List.tail l)) minimum (st-1)
                   else if(c<=o && c<=max && c<=min) then
                       if(c<minimum) then
                           najdi (Maybe.withDefault [] (List.tail l)) c (st-1)
                       else
                           najdi (Maybe.withDefault [] (List.tail l)) minimum (st-1)
                   else if(max<=o && max<=c && max<=min) then
                       if(max<minimum) then
                           najdi (Maybe.withDefault [] (List.tail l)) max (st-1)
                       else
                           najdi (Maybe.withDefault [] (List.tail l)) minimum (st-1)
                   else
                       if(min<minimum) then
                           najdi (Maybe.withDefault [] (List.tail l)) min (st-1)
                       else
                           najdi (Maybe.withDefault [] (List.tail l)) minimum (st-1)

    in
        najdi l 900000000 stSveck


--funkcija, ki vrne faktor za razmerje vi�ine
getHighest : List Candle -> Int -> Float
getHighest l stSveck=
    let find l maximum st =
        if(List.isEmpty l)then
            maximum
        else if(st==0) then
            maximum
        else
            case (Maybe.withDefault Empty (List.head l)) of
               Empty -> maximum
               Candle o c max min ->
                   if(o>=c && o>=max && o>=min) then
                       if(o>maximum) then
                           find (Maybe.withDefault [] (List.tail l)) o (st-1)
                       else
                           find (Maybe.withDefault [] (List.tail l)) maximum (st-1)
                   else if(c>=o && c>=max && c>=min) then
                       if(c>maximum) then
                           find (Maybe.withDefault [] (List.tail l)) c (st-1)
                       else
                           find (Maybe.withDefault [] (List.tail l)) maximum (st-1)
                   else if(max>=o && max>=c && max>=min) then
                       if(max>maximum) then
                           find (Maybe.withDefault [] (List.tail l)) max (st-1)
                       else
                           find (Maybe.withDefault [] (List.tail l)) maximum (st-1)
                   else
                       if(min>maximum) then
                           find (Maybe.withDefault [] (List.tail l)) min (st-1)
                       else
                           find (Maybe.withDefault [] (List.tail l)) maximum (st-1)

    in
        find l 0 stSveck

--Funkcija, ki nari�e navpi�no �rto
drawLine : Float -> Float -> Float -> Float -> Float -> Float -> Float -> Float -> (Svg.Svg msg)
drawLine sirina x y a b high low height=
    let
        iks = (toString(x+(sirina/2)))
        ipsilon1 = (toString (y-((a-low)*((height-10)/(high-low)))))
        ipsilon2 = (toString (y-((b-low)*((height-10)/(high-low)))))
    in
        Svg.line [x1 iks,x2 iks, y1 ipsilon1 ,y2 ipsilon2, stroke "black"] []

{-===============================================================================-}
{-============================= KONEC GRAFA =====================================-}
{-===============================================================================-}
