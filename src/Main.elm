module Main exposing (..)

import Markets exposing (..)
import History exposing (..)
import Graph exposing (..)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onClick, onInput)
import Http exposing (..)
import Json.Decode exposing (field, string, list)
import Round exposing (..)
import Svg
import Svg.Attributes exposing (..)
import WebSocket exposing (..)
import Time exposing (Time)
import Date
import Task

{-===============================================================================-}
{-================================= HTML ========================================-}
{-===============================================================================-}

stylesheet =
    let
        tag =
            "link"

        attrs =
            [ attribute "Rel" "stylesheet"
            , attribute "property" "stylesheet"
            , attribute "href" "bootstrap.css"
            ]

        children =
            []
    in
        node tag attrs children



htmlContainer : Model -> Html Msg
htmlContainer model =
    div [Html.Attributes.class "table" ,Html.Attributes.class "table-hover"]
        [ htmlSearch
        , htmlTable (List.take model.perPage (List.drop (model.perPage * model.page) (List.filter (\x -> x.isActive) model.coins)))
        , htmlNextPrev
         ]

htmlSearch : Html Msg
htmlSearch =
  div [] [
   div [ Html.Attributes.class "hodl"] [text "hodl"]
  ,div [Html.Attributes.class "btn",Html.Attributes.class "col-sm-8"]
      [input [ onInput Search, placeholder "Search" ,Html.Attributes.class "form-control",Html.Attributes.class "col-sm-5"]
        []
        ]
  ]

htmlNextPrev : Html Msg
htmlNextPrev =
    div []
        [
         button [ onClick (NextPage False)]
            [ text "Prev" ]
        , button [onClick (NextPage True) ]
            [ text "Next" ]
        ]


htmlTable : List Coin -> Html Msg
htmlTable l =
    table [Html.Attributes.class "col-sm-12"]
    [ thead []
        [ tr []
            [ th []
                [ text "Logo" ]
            ,th [ onClick (Sort "Name") ]
                [ text "Coin" ]
            , th [ onClick (Sort "Price") ]
                [ text "Price (BTC)" ]
            , th [ onClick (Sort "Volume") ]
                [ text "Volume (BTC)" ]
            , th [ onClick (Sort "Change") ]
                [ text "Change (24h)" ]
            ]
        ]
    , tbody []
        (List.map (htmlRow) (l))
    ]

genericLogo : Maybe String -> String
genericLogo url =
    case url of
        Nothing ->
            "https://thebitcoin.pub/uploads/default/original/2X/1/1436aa2413ab7c6ec26e9b17031ba76815585a8c.png"
        Just url ->
            if url == "https://bittrex.com/Content/img/symbols/BTC.png" then
                "https://thebitcoin.pub/uploads/default/original/2X/1/1436aa2413ab7c6ec26e9b17031ba76815585a8c.png"
            else
                url

htmlRow : Coin -> Html Msg
htmlRow c =
    tr [ onClick (ShowGraph c.nameShort) ]
        [ td []
            [ img [ src (genericLogo c.logo), alt c.nameShort, Html.Attributes.style [("height", "32px"), ("width", "32px")] ] [ ] ]
        ,td []
            [ text c.nameShort ]
        , td []
            [ text (Round.round 8 c.priceLast) ]
        , td []
            [ text (Round.round 1 c.volume) ]
        , td []
            [ text ((Round.round 1 ((c.priceLast/c.pricePrevDay - 1.0) * 100))++"%") ]
        ]


htmlCoinInfo : Model -> Html Msg
htmlCoinInfo model =
    div [(color (Maybe.withDefault "" model.openedCoin) model)]
        [
            Html.text ((Maybe.withDefault "" model.openedCoin)
            ++ " "
            ++ Round.round 8 (getPrice (model.openedCoin) model.tickers -1.0) )

        ]
color : String -> Model -> Attribute msg
color name  model =
    case (getCoin name model) of
      Nothing ->
          Html.Attributes.class "btn btn-danger"

      Just a ->
            if a.pricePrevDay > a.priceLast then
              Html.Attributes.class "btn btn-danger"
            else
              Html.Attributes.class "btn btn-success"

getCoin : String -> Model -> Maybe Coin
getCoin name model =
    let
      f : Coin -> Maybe Coin -> Maybe Coin
      f el ac =
        if el.nameShort == name then
          Just el
        else
          ac
    in

      if name == "" then
        List.head model.coins

      else
        -- List.foldl (\a b -> if a.nameShort == name then (Maybe.withDefault nekCoin a) else (Maybe.withDefault nekCoin b)) (nekCoin) model.coins}
        List.foldl (f) (Nothing) (model.coins)


htmlButtonBack : Html Msg
htmlButtonBack =
    div []
        [button [Html.Attributes.class "btn", onClick CloseGraph ]
            [ Html.text "Back" ]
        ]

htmlControlPanel : Html Msg
htmlControlPanel =
    div [Html.Attributes.class "btn-toolbar"]
        [
        button [Html.Attributes.class "btn btn-danger btn-sm" ,onClick (Zoom True) ]
            [ Html.text "-" ]
        , button [Html.Attributes.class "btn btn-success btn-sm", onClick (Zoom False) ]
            [ Html.text "+" ]
        , button [Html.Attributes.class "btn1 btn-info btn-sm", onClick (CandleTime 24) ]
            [ Html.text "1d" ]
        , button [Html.Attributes.class "btn1 btn-info btn-sm", onClick (CandleTime 4) ]
            [ Html.text "4h" ]
        , button [Html.Attributes.class "btn1 btn-info btn-sm", onClick (CandleTime 1) ]
            [ Html.text "1h" ]
        , button [Html.Attributes.class "btn1 btn-info btn-sm"]
            [ Html.text "SMA" ]
        , button [Html.Attributes.class "btn1 btn-info btn-sm"]
            [ Html.text "EMA" ]
        , button [Html.Attributes.class "btn1 btn-info btn-sm"]
            [ Html.text "EIS" ]
        ]

convertCandle : List Candlestick -> Int -> List Candlestick
convertCandle l time =
    let
        f : Candlestick -> (List Candlestick, Int) -> (List Candlestick, Int)
        f c a =
            case a of
                (h::t, n) ->
                    if n == time then
                        (c::h::t, n - 1)
                    else if n > 0 then
                        (
                            { h
                            | high = (if c.high > h.high then c.high else h.high)
                            , low = (if c.low < h.low then c.low else h.low)
                            , volume = (h.volume + c.volume)
                            }::t
                            , n - 1
                        )
                    else
                        (
                            { h
                            | close = c.close
                            , high = (if c.high > h.high then c.high else h.high)
                            , low = (if c.low < h.low then c.low else h.low)
                            , volume = (h.volume + c.volume)
                            }::t
                            , time
                        )
                ([], n) ->
                    ([c], n - 1)
    in
        if time == 1 then
            l
        else
            case List.foldl (f) ([], time) (l) of
                (a, b) ->
                    List.reverse a

svgGraph : Model -> Html Msg
svgGraph model =
    let
        editCandles : List Candlestick -> List Candlestick
        editCandles l =
            List.drop (List.length l - model.stSveck) l
    in
        Svg.svg [ Svg.Attributes.width (toString 1500)
                , Svg.Attributes.height (toString 700)
                , stroke "black", fill "blue"
                ]
            (
                ( Graph.drawEMA (List.map (\x->0.18) model.history) model.stSveck 1100 550)
                ++
                ( Graph.draw (Graph.convert (editCandles (convertCandle model.history model.candleTime))) (model.stSveck) 1100 550)
                )

svgEma : Model -> Html Msg
svgEma model =
    Svg.svg [ Svg.Attributes.width (toString 1500)
                , Svg.Attributes.height (toString 700)
                , stroke "black", fill "blue"
                ]
            ( Graph.drawSMA (List.take model.stSveck (calcSma 9 model.history)) model.stSveck 1500 550 )

coinPreviewPage : Model -> Html Msg
coinPreviewPage model =
    div []
        [htmlButtonBack
        , htmlCoinInfo model
        , htmlControlPanel
        , svgGraph model
        ]

todoItem : String -> Html Msg
todoItem todo =
    li [Html.Attributes.class "list-group-item" ] [ text todo, button [ onClick (RemoveItem todo), Html.Attributes.class "btn-info", Html.Attributes.class "pull-right" ] [ text "x" ] ]

todoList : List String -> Html Msg
todoList todos =
    let
        child =
            List.map todoItem todos
    in
        ul [ Html.Attributes.class "list-group" ] child

opomnik : Model -> Html Msg
opomnik model =
    div [][
      div [Html.Attributes.class "container-fluid", Html.Attributes.class "h5"] [text "Note"]
      ,div [Html.Attributes.class "col-sm-5"]
          [input
              [ Html.Attributes.type_ "text"
              , onInput UpdateTodo
              , Html.Attributes.class "form-control"
              , value model.todo
              ,  Html.Events.onMouseEnter ClearInput
              ]
              []
          , button [ onClick AddTodo, Html.Attributes.class "btn-primary" ] [ text "Add" ]
          , button [ onClick RemoveAll, Html.Attributes.class "btn-danger" ] [ text "Delete all" ]
          , todoList model.todos
          ]
      ]
{-===============================================================================-}
{-================================= MODEL =======================================-}
{-===============================================================================-}

type alias Tick =
    { market : String
    , price : Float
    }

-- MODEL
type alias Model =
    { coins : List Coin
    , sortedBy : String
    , page : Int
    , perPage : Int
    , history : List Candlestick
    , showGraph : Bool
    , openedCoin : Maybe String
    , stSveck : Int
    , candleTime : Int
    , tickers : List Tick
    , newHour : Bool
    , todo : String
    , todos : List String
    }

-- MSG
type Msg
    = ShowGraph String
    | CloseGraph
    | Sort String
    | NextPage Bool
    | Search String
    | GetMarkets (Result.Result Http.Error String)
    | GetHistory (Result.Result Http.Error String)
    | Zoom Bool
    | CandleTime Int
    | OnGetTickers String -- when we get new message over socket
    | OnTime Time
    | UpdateTodo String-- opomnik
    | AddTodo
    | RemoveItem String
    | RemoveAll
    | ClearInput

loadMarkets =
    Http.send GetMarkets (Http.getString Markets.marketsUrl)

loadCoinHistory coin =
    Http.send GetHistory (Http.getString (History.coinUrl coin))

-- INIT
init : (Model, Cmd Msg)
init =
    (
        { coins = []
        , sortedBy = "Price"
        , page = 00
        , perPage = 10
        , history = []
        , showGraph = False
        , stSveck = 75
        , candleTime = 1
        , tickers = []
        , openedCoin = Nothing
        , newHour = False
        , todo = ""
        , todos = []
        }
    , loadMarkets)

-- VIEW
view : Model -> Html Msg
view model =
    div [Html.Attributes.class "container"] [
      stylesheet
      ,case (model.showGraph, model.coins) of
          (True , a::b) ->
              coinPreviewPage model
          (False, a::b) ->
              htmlContainer model
          (_, []) ->
              -- TODO: No results
              htmlContainer model
      ,
        opomnik model
      ]
-- UPDATE
update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        ShowGraph coin ->
            ( { model | showGraph = True, openedCoin = (Just coin) }, (loadCoinHistory coin) )
        Sort category ->
            ( { model | coins = (sortByCategory model category), sortedBy = category, page = 0}, Cmd.none)
        NextPage b ->
            ( { model | page = (switchPage model b) }, Cmd.none )
        Search str ->
            ( { model | coins = (filterSearch model str) }, Cmd.none )
        GetMarkets (Ok response) ->
            ( { model | coins = (Markets.coins response)}, Cmd.none )
        GetMarkets (Err _) ->
            ( { model | coins = [Coin "Err" "" "" (Just "") True 0 0 0] }, Cmd.none )
        GetHistory (Ok response) ->
            ( { model | history = (History.candles response) }, Cmd.none )
        GetHistory (Err _) ->
            ( model, Cmd.none )
        CloseGraph ->
            ( { model | showGraph = False, openedCoin = Nothing, history = [] }, Cmd.none )
        Zoom True ->
            ( { model | stSveck = (if model.stSveck < 150 then model.stSveck + 5 else model.stSveck) }, Cmd.none )
        Zoom False ->
            ( { model | stSveck = (if model.stSveck > 10 then model.stSveck - 5 else model.stSveck) }, Cmd.none )
        CandleTime time ->
            ( { model | candleTime = time }, Cmd.none )
        OnGetTickers ticks ->
            ( updateLstCandle model ticks, getTime )
        OnTime time ->
            if (Basics.round (Time.inMinutes time)%60) == 0 then
                if model.newHour then
                    ( { model | history = addNewCandle model, newHour = False }, Cmd.none )
                else
                    ( model, Cmd.none )
            else
                ( { model | newHour = True }, Cmd.none )

        --Opomnik
        UpdateTodo text ->
            ({ model | todo = text }, Cmd.none)

        AddTodo ->
            ({ model | todos = model.todo :: model.todos }, Cmd.none)

        RemoveItem text ->
            ({ model | todos = List.filter (\x -> x /= text) model.todos }, Cmd.none)

        RemoveAll ->
            ({ model | todos = [] }, Cmd.none)

        ClearInput ->
            ({ model | todo = "" }, Cmd.none)

addNewCandle : Model -> List Candlestick
addNewCandle model =
    case (List.reverse model.history) of
        h::t ->
            case (getPrice model.openedCoin model.tickers -1.0) of
                -1.0 ->
                    model.history
                price ->
                    List.reverse ((Candlestick
                    (price)
                    (price)
                    (price)
                    (price)
                    (0.0)
                    )::h::t)
        [] ->
            []

getTime =
    Time.now
        |> Task.perform OnTime

getPrice : Maybe String -> List Tick -> Float -> Float
getPrice coin lt current =
    case coin of
        Nothing ->
            current
        _ ->
            List.foldl (\el ac -> if el.market == ("BTC-"++(Maybe.withDefault "" coin)) then el.price else ac) (current) (lt)

updateLstCandle : Model -> String -> Model
updateLstCandle model ticks =
    let
        update : Model -> List Candlestick -> List Tick -> List Candlestick
        update model lc lt =
            case (List.reverse lc) of
                h::t ->
                    case (getPrice model.openedCoin lt h.close) of
                        price ->
                            List.reverse
                            ({ h
                            | close = price
                            , high = (if price > h.high then price else h.high)
                            , low = (if price < h.low then price else h.low)
                            , open = (if h.open == -1.0 then price else h.open)
                            }::t)
                [] ->
                    lc
    in
        case decodeTicks ticks of
            h::t ->
                { model
                | history =  (update model model.history (h::t))
                , tickers = h::t
                }
            _ ->
                { model | history = [] }


decodeTicks : String -> List Tick
decodeTicks ticks =
    case Json.Decode.decodeString (Json.Decode.list tickDecoder) (ticks) of
        Ok a ->
            a
        Err a ->
            []

tickDecoder =
    Json.Decode.map2
        Tick
        ( Json.Decode.at ["market"] Json.Decode.string )
        ( Json.Decode.at ["price"] Json.Decode.float )

-- SUBSCRIPTIONS
subscriptions : Model -> Sub Msg
subscriptions model =
  WebSocket.listen "ws://83.212.82.20:1337" OnGetTickers

filterSearch : Model -> String -> List Coin
filterSearch model str =
    let
        check x =
            (String.contains (String.toLower str) (String.toLower x.nameShort))
            || (String.contains (String.toLower str) (String.toLower x.nameLong))
    in
    List.map ( \x -> { x | isActive = (check x) } ) model.coins

numOfActive : List Coin -> Int
numOfActive l =
    List.foldl (\x a -> if x.isActive then a + 1 else a) (0) (l)

switchPage : Model -> Bool -> Int
switchPage model b =
    if b == False && model.page == 0  then
            model.page
    else if b && (((model.page + 1) * model.perPage) >= (numOfActive model.coins)) then
        model.page
    else
        if b then
            model.page + 1
        else
            model.page - 1


sortByCategory : Model -> String -> List Coin
sortByCategory model category =
    if model.sortedBy == category then
        List.reverse model.coins
    else
        case category of
            "Name" ->
                List.sortBy .nameShort model.coins
            "Price" ->
                List.sortBy .priceLast model.coins
            "Volume" ->
                List.sortBy .volume model.coins
            "Change" ->
                List.sortBy (\x -> x.pricePrevDay/x.priceLast) model.coins
            _ ->
                model.coins

-- calcEma : Int -> List Candlestick -> List Float

calcSma : Int -> List Candlestick -> List Float
calcSma n lc =
    let
        f : Candlestick -> (List Float, List Candlestick, Int) -> (List Float, List Candlestick, Int)
        f c a =
            case a of
                (x, y, 1) ->
                    ((0.1)::x, (List.drop 1 y), 1)
                (x, y, z) ->
                    ((-1.0)::x, y, z-1)
    in
        -- case List.foldl (f) ([], lc, n) (lc) of
        --     (l, b, a) ->
        --         List.reverse l
        List.map (\x -> 0.18) lc

-- MAIN
main =
    Html.program
        { init = init
        , update = update
        , view = view
        , subscriptions = subscriptions
        }
